package com.sukrit.cruxproject.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sukrit.cruxproject.R;
import com.sukrit.cruxproject.app.CustomApplication;
import com.sukrit.cruxproject.helper.PreferenceHelper;
import com.sukrit.cruxproject.helper.SwipeDetectorLayout;
import com.sukrit.cruxproject.model.XkcdResponse;
import com.sukrit.cruxproject.service.XkcdAPI;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements Callback<XkcdResponse> {

    /*
     TODO:
      SWIPE FEATURE
     */

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.imageView)
    PhotoView imageView;

    @BindView(R.id.fabForward)
    FloatingActionButton fabForward;

    @BindView(R.id.constraintLayout)
    SwipeDetectorLayout swipeDetectorLayout;

    @BindView(R.id.fabBack)
    FloatingActionButton fabBack;

    @OnClick(R.id.fabForward)
    public void loadNext(){
        if((current + 1)<= MAX){
            loadSpecific(++current);
        }else{
            Toast.makeText(this,"Reached End",Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.fabBack)
    public void loadPrevious() {
        if((current- 1) <= 0){
            Toast.makeText(this,"Reached Start",Toast.LENGTH_LONG).show();
        }else{
            loadSpecific(--current);
        }
    }

    @OnLongClick(R.id.imageView)
    public void openSelfText(){
        if(xkcdComic !=  null){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("SELF TEXT")
                    .setMessage(xkcdComic.getAlt())
                    .setNegativeButton("Dismiss", (dialogInterface, i) -> dialogInterface.dismiss());
            AlertDialog dialog = builder.create();
            dialog.show();
        }   else {
            Toast.makeText(this,"Error Opening Self Text",Toast.LENGTH_SHORT).show();
        }
    }

    XkcdAPI xkcdAPI;
    PreferenceHelper preferenceHelper;
    private static final int REQUEST_STORAGE = 10;
    Bitmap bitmap;
    boolean isFavourite = false;
    boolean isEmpty = true;


    XkcdResponse xkcdComic;
    List<String> favNum;
    String current_img_link =  "";
    String current_name = "";
    int current;
    MenuItem favItem;
    private int MAX = 2202;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        preferenceHelper = new PreferenceHelper(this);
        current = preferenceHelper.getCurrent();

        favNum = new ArrayList<>();
        favNum = preferenceHelper.getFavNum();
        if(!favNum.isEmpty()){
            isEmpty = false;
        }

        CustomApplication customApplication = CustomApplication.get(this);
        MAX = customApplication.getMAX_COMIC();
        xkcdAPI = customApplication.xkcdAPI();

        if(current == 0) {
            getLatest();
        }else{
            loadSpecific(current);
        }



        swipeDetectorLayout.setSwipeEventsListener(TYPE -> {
            if(TYPE == 1){
                loadNext();
            }else if (TYPE == 2){
                loadPrevious();
            }
        });

    }

    @Override
    protected void onResume() {
        if(current_name.length() > 0) {
            updateList();
            isFavourite = isFav(String.valueOf(current));
            updateMenu();
        }
        super.onResume();
    }


    private boolean isFav(String num){
        if(!isEmpty){
            int flag = 0;
            for(String t : favNum){
                if(num.equals(t)){
                    flag = 1;
                }
            }
            return flag == 1;
        }else{
            return false;
        }
    }

    private boolean isWritable(Context context){
        return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }


    private void loadSpecific(int num){
        Call<XkcdResponse> call = xkcdAPI.getSpecific(num);
        preferenceHelper.setCurrent(num);
        call.enqueue(this);
        if(num == MAX){
            fabForward.hide();
        }else if(num == 1){
            fabBack.hide();
        }else{
            fabForward.show();
            fabBack.show();
        }
    }

    private void getLatest(){
        Call<XkcdResponse> call = xkcdAPI.getHome();
        call.enqueue(this);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        favItem = menu.findItem(R.id.action_fav);
        return true;
    }

    void openWebpage(Uri uri){
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(ContextCompat.getColor(this,R.color.colorPrimary));
        CustomTabsIntent tabsIntent = builder.build();
        tabsIntent.launchUrl(this,uri);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_browser) {
            openWebpage(Uri.parse("http://xkcd.com/"+ current));
        }else if(id == R.id.action_fav){
            if(!isFavourite){
                //favItem = item;
                //item.setTitle(R.string.action_favourite_remove);
                if(xkcdComic != null){
                    preferenceHelper.addComic(xkcdComic);
                }
                isFavourite = true;
                updateMenu();
                updateList();
                Toast.makeText(this,"Added to favourite",Toast.LENGTH_LONG).show();
            }else{
                preferenceHelper.removeComic(xkcdComic.getTitle());
                isFavourite = false;
                updateMenu();
                updateList();
                Toast.makeText(this,"Removed from favourite",Toast.LENGTH_LONG).show();
            }
        }else if (id == R.id.action_save){
            if (isWritable(this)) {
                saveCurrent();
            }else{
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_STORAGE);
            }
        }else if (id == R.id.action_random){
            Random random = new Random();
            current = random.nextInt(MAX);
            loadSpecific(current);
        }else  if( id == R.id.action_list){
            Intent intent = new Intent(this, FavouriteActivity.class);
            startActivityForResult(intent,1);
        }else if (id == R.id.action_specific){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_specific,null);
            builder.setView(dialogView);
            EditText editText = (EditText) dialogView.findViewById(R.id.editText);
            builder.setTitle("Open Specific Comic ")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if(editText != null){

                                int select = Integer.parseInt(editText.getText().toString());

                                if(select <= MAX && select >= 1){
                                    current = select;
                                    loadSpecific(current);
                                    if(select == 42){
                                        Toast.makeText(MainActivity.this,"Loading the Answer to the Ultimate Question of Life, the Universe, and Everything",Toast.LENGTH_SHORT).show();
                                    }
                                    dialogInterface.dismiss();
                                }else{
                                    Toast.makeText(MainActivity.this,"Invalid Comic Number",Toast.LENGTH_SHORT).show();
                                    dialogInterface.dismiss();
                                }
                            }else{
                                Toast.makeText(MainActivity.this,"Unknown Error",Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

            AlertDialog dialog = builder.create();
            dialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateMenu(){
        if (favItem != null){
            if(isFavourite){
                favItem.setTitle(R.string.action_favourite_remove);
            }   else {
                favItem.setTitle(R.string.action_favourite);
            }
        }
    }

    private void updateList(){
        favNum = preferenceHelper.getFavNum();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode == RESULT_OK && data != null){
            int toLoad = Integer.parseInt(data.getStringExtra("COMIC_NUM"));
            if(toLoad <= MAX){
                current = toLoad;
                loadSpecific(toLoad);
            }else{
                Toast.makeText(this,"Unknown Error Occurred",Toast.LENGTH_LONG).show();
            }
        }
    }

    private void updateUi(XkcdResponse xkcdResponse){
        isFavourite = false;
        xkcdComic = xkcdResponse;
        isFavourite = isFav(xkcdComic.getNum());
        updateMenu();
        Glide.with(this)
                .load(xkcdResponse.getImg())
                .placeholder(R.drawable.placeholder)
                .addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        handleError();

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        bitmap = ((BitmapDrawable)resource).getBitmap();
                        return false;
                    }
                })
                .into(imageView);
        current_img_link = xkcdResponse.getImg();
        current_name = xkcdResponse.getTitle();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(xkcdResponse.getTitle());
            getSupportActionBar().setSubtitle("#" + xkcdResponse.getNum()+ "/" + MAX );
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_STORAGE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                saveCurrent();
            } else {
                Toast.makeText(this, "Storage Permission not granted error ! ", Toast.LENGTH_LONG).show();
            }
        }
    }

    void saveComic(Bitmap bitmap){
        boolean success =false;
        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/XkcdComics";
        File dir = new File(file_path);
        if(!dir.exists()){
            dir.mkdir();
        }
        if(current_name.length() > 0){
            File file = new File(dir,current_name+".png");
            FileOutputStream fOut = null;
            try {
                fOut = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG,100,fOut);
                fOut.flush();
                fOut.close();
                success = true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(success){
                Toast.makeText(this,"Saved Comic",Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this,"Error Saving Comic",Toast.LENGTH_LONG).show();
            }
        }
    }

    void saveCurrent(){
        if(current_img_link.length() > 0){
            if(bitmap != null){
                saveComic(bitmap);
            }else{
                handleError();
            }
        }else{
            handleError();
        }
    }

    void handleError(){
        Toast.makeText(this,"Error loading comic",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(Call<XkcdResponse> call, Response<XkcdResponse> response) {
        if(response.isSuccessful()){
            if (response.body() != null) {
                updateUi(response.body());
            }else{
                handleError();
            }
        }else{
            handleError();
        }
    }

    @Override
    public void onFailure(Call<XkcdResponse> call, Throwable t) {
        Toast.makeText(this,"Error",Toast.LENGTH_LONG).show();
    }
}
